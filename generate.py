import asyncio
from urllib import parse

from aiofile import async_open

from lolapi import get_match_info, get_server, get_summoner_info

_LANE = {
    "TOP": "top",
    "JUNGLE": "jg",
    "MIDDLE": "mid",
    "BOTTOM": "bot",
    "UTILITY": "sup",
}


TEMPLATE = """Match Info:
{log_url}

Blue Side:
Top - {blue_top_opgg_url} ({blue_top_rank})
Jungle - {blue_jg_opgg_url} ({blue_jg_rank})
Mid - {blue_mid_opgg_url} ({blue_mid_rank})
Bot - {blue_bot_opgg_url} ({blue_bot_rank})
Support - {blue_sup_opgg_url} ({blue_sup_rank})

Red Side:
Top - {red_top_opgg_url} ({red_top_rank})
Jungle - {red_jg_opgg_url} ({red_jg_rank})
Mid - {red_mid_opgg_url} ({red_mid_rank})
Bot - {red_bot_opgg_url} ({red_bot_rank})
Support - {red_sup_opgg_url} ({red_sup_rank})"""


data = {
    "log_url": "",
    "blue_top_opgg_url": "",
    "blue_top_rank": "",
    "blue_jg_opgg_url": "",
    "blue_jg_rank": "",
    "blue_mid_opgg_url": "",
    "blue_mid_rank": "",
    "blue_bot_opgg_url": "",
    "blue_bot_rank": "",
    "blue_sup_opgg_url": "",
    "blue_sup_rank": "",
    "red_top_opgg_url": "",
    "red_top_rank": "",
    "red_jg_opgg_url": "",
    "red_jg_rank": "",
    "red_mid_opgg_url": "",
    "red_mid_rank": "",
    "red_bot_opgg_url": "",
    "red_bot_rank": "",
    "red_sup_opgg_url": "",
    "red_sup_rank": "",
}


def generate_opgg_url(name: str) -> str:
    url = f"https://op.gg/summoners/{get_server()}/{parse.quote(name)}"
    return url


async def generate(match_id: int) -> str:
    data["log_url"] = f"https://leagueofgraphs.com/match/na/{match_id}"
    match_info = await get_match_info(match_id)
    participants = match_info["info"]["participants"]

    for index, participant in enumerate(participants):
        summoner_id = participant["summonerId"]
        name, rank = await get_summoner_info(summoner_id)
        team_position = participant["teamPosition"]
        lane = _LANE[team_position]
        side = "blue" if 0 <= index <= 4 else "red"
        data[f"{side}_{lane}_opgg_url"] = generate_opgg_url(name)
        data[f"{side}_{lane}_rank"] = rank

    async with async_open("./description.txt", "w") as file:
        await file.write(TEMPLATE.format(**data))
    return


if __name__ == "__main__":
    match_id = int(input("Please enter the match ID: "))
    loop = asyncio.get_event_loop()
    loop.run_until_complete(generate(match_id))