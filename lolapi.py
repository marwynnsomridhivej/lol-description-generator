import os

import aiohttp
from dotenv import load_dotenv

load_dotenv("./.env")

API_KEY = os.getenv("RGAPIKEY")
REGION = os.getenv("REGION")
SERVER = os.getenv("SERVER")

# Base URL format of region/server, endpoint
_BASE = "https://{}.api.riotgames.com/lol/{}"
_RANK = {
    "I": 1,
    "II": 2,
    "III": 3,
    "IV": 4,
}
_SERVERS = {
    "BR1": "br",
    "EUN1": "eune",
    "EUW1": "euw",
    "JP1": "jp",
    "KR": "kr",
    "LA1": "lan",
    "LA2": "las",
    "NA1": "na",
    "OC1": "oce",
    "TR1": "tr",
    "RU": "ru",
    "PH2": "ph",
    "SG2": "sg",
    "TH2": "th",
    "TW2": "tw",
    "VN2": "vn",
}


def _convert(rank: str) -> int:
    return _RANK[rank]


async def _get(endpoint: str, data: dict = {}, use_server: bool = True) -> dict:
    finalised_endpoint = endpoint
    data["api_key"] = API_KEY
    finalised_endpoint += "?" + \
        "&".join([f"{key}={value}" for key, value in data.items()])

    url = _BASE.format(SERVER if use_server else REGION, finalised_endpoint)
    print(url)

    async with aiohttp.ClientSession() as cs:
        async with cs.get(url) as request:
            response = await request.json()
    return response


def get_server() -> str:
    return _SERVERS[SERVER]


async def get_match_info(match_id: int) -> dict:
    formatted_match_id = f"{SERVER}_{match_id}"
    return await _get(f"match/v5/matches/{formatted_match_id}", use_server=False)


async def get_summoner_info(summoner_id: str) -> tuple[str, str]:
    ranked_data = await _get(f"league/v4/entries/by-summoner/{summoner_id}")
    if ranked_data:
        for queue in ranked_data:
            if queue["queueType"] == "RANKED_SOLO_5x5":
                return queue["summonerName"], f"{queue['tier']} {_convert(queue['rank'])}".title()

    level_data = await _get(f"summoner/v4/summoners/{summoner_id}")
    return level_data["name"], f"Level {level_data['summonerLevel']}"
